import os
import sys
import copy
import random
import threading

from player import play
from xoroshiro import Xoroshiro128
from othello import Board, BLACK, WHITE, EMPTY

TIMEOUT = 1.0
SERVER = os.environ['SERVER'] if 'SERVER' in os.environ else 0

def read_move():
  if SERVER:
    r, c = map(int, input().split())
  else:
    s = input()
    r, c = ord(s[0]) - ord('1'), ord(s[1]) - ord('a')
  return (r, c)

def print_move(r, c):
  if not SERVER:
    x, y = chr(r + ord('1')), chr(c + ord('a'))
    print(x + y)
  else:
    print(r, c)
    sys.stdout.flush()

def report_winner(board):
  if not SERVER:
    if board.winner() == BLACK:
      print("Black wins!")
    else:
      print("White wins!")
  else:
    print(0 if board.winner() == BLACK else 1)
    sys.stdout.flush()

def main():
  if len(sys.argv) < 2:
    sys.stderr.write('[Usage] ./main.py whoami\n')
    sys.exit(0)

  rng = Xoroshiro128()
  board = Board()
  whoami = None
  if int(sys.argv[1]) == 0:
    whoami = BLACK
  else:
    whoami = WHITE

  def rand_move(board):
    pos = []
    for i in range(8):
      for j in range(8):
        if board.valid(i, j):
          pos.append((i, j))
    return pos[rng() % len(pos)]

  def main_play(board):
    try:
      res = play(board)
      return res
    except Exception as e:
      print(e)
      return rand_move(board)

  if not SERVER:
    print(board)
  while not board.terminated():
    if board.skip():
      continue

    r, c = -1, -1

    from multiprocessing.pool import ThreadPool, TimeoutError
    pool = ThreadPool(processes=1)

    if board.cur_player() == whoami:
      fut = pool.apply_async(main_play, (copy.deepcopy(board),))
      r, c = None, None
      try:
        r, c = fut.get(TIMEOUT)
      except TimeoutError:
        if not SERVER:
          print('Timeout')
        pool.terminate()
        r, c = rand_move(board)
      except Exception as e:
        if not SERVER:
          print('Error: ', e)
        r, c = rand_move(board)
      print_move(r, c)
    else:
      r, c = read_move()
    if not board.valid(r, c):
      r, c = rand_move(board)
      if not SERVER:
        print("Invalid action. Randomly place at ", end='')
        print_move(r, c)
    board.place(r, c)
    if not SERVER:
      print(board)
  report_winner(board)

if __name__ == '__main__':
  main()
