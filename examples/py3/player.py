import random
import time
import copy

from othello import *

"""
Please write your Othello AI here.
Please do not touch any other functions.
"""

# This is a greedy AI which greedily choose the move 
# which leads to the maximum value defined by the evaluate() function.

def evaluate(board, player):
  # the value matrix indicates the points one will get 
  # if the corresponding cell is occupied
  value = [
    [3, 2, 2, 2, 2, 2, 2, 3],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [3, 2, 2, 2, 2, 2, 2, 3]
  ]
  points = 0
  for i in range(8):
    for j in range(8):
      if board.get(i, j) == player: # if I occupy this cell
        points += value[i][j]
      elif board.get(i, j) == get_opposite(player): # if the opponent occupies this cell
        points -= value[i][j]
  return points

def play(board):
  player = board.cur_player() # player is the current player
  pos = board.get_available_moves() # pos contains all valid moves
  best_move = None # best_move is the move which leads to maximum profit
  best_eval = 0 # the maximum points far
  for p in pos: # iterate over the valid moves
    board2 = copy.deepcopy(board) # board2 is a copy of board
    board2.place(p) # place at p
    if board2.terminated(): # if the game is terminated
      if board2.winner() == player: # find winning move
        best_move = p
      continue
    opponent = board2.cur_player() # opponent player
    next_pos = board2.get_available_moves() # next_pos contains all valid moves for the opponent
    best_move_for_opponent = None
    best_eval_for_opponent = 0
    for p2 in next_pos: # iterate over opponent's valid moves
      board3 = copy.deepcopy(board2) # board3 is a copy of board2
      board3.place(p2)
      if best_move_for_opponent is None or evaluate(board2, opponent) > best_eval_for_opponent: # similar to the py2 example
        best_eval_for_opponent = evaluate(board2, opponent)
        best_move_for_opponent = p2
    if best_move is None or -best_eval_for_opponent > best_eval: # minimax
      best_eval = -best_eval_for_opponent
      best_move = p
  assert best_move is not None
  return best_move # return the optimal move
