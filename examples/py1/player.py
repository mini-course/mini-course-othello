import random
import time
import copy

"""
Please write your Othello AI here.
Please do not touch any other functions.
"""

# This is a greedy AI which greedily choose the move 
# which leads to the maximum number of disks of the current player.
def play(board):
  player = board.cur_player() # player is the current player
  pos = board.get_available_moves() # pos contains all valid moves
  best_move = None # best_move is the move which leads to maximum profit
  best_cnt = 0 # the maximum number of disks so far
  for p in pos: # iterate over the valid moves
    board2 = copy.deepcopy(board) # board2 is a copy of board
    board2.place(p) # place at p
    if board2.count(player) > best_cnt: # if this move leads to more disks
      best_cnt = board2.count(player) # update the counter
      best_move = p # update the optimal move
  return best_move # return the optimal move
