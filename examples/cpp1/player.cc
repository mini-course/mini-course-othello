#include "player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <utility>
#include <vector>

// This is a greedy AI which greedily choose the move
// which leads to the maximum number of disks of the current player.
Position Play(Board board) {
  auto player = board.CurPlayer();       // player is the current player
  auto pos = board.GetAvailableMoves();  // pos contains all valid moves
  Position best_move;  // best_move is the move which leads to maximum profit
  int best_cnt = 0;    // the maximum number of disks so far
  for (Position p : pos) {                  // iterate over the valid moves
    Board board2 = board;                   // board2 is a copy of board
    board2.Place(p);                        // place at p
    if (board2.Count(player) > best_cnt) {  // if this move leads to more disks
      best_cnt = board2.Count(player);      // update the counter
      best_move = p;                        // update the optimal move
    }
  }
  return best_move;  // return the optimal move
}
