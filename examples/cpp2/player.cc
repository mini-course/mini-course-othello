#include "player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <utility>
#include <vector>

// This is a greedy AI which greedily choose the move
// which leads to the maximum value defined by the Evaluate() function.

int Evaluate(Board board, CellType player) {
  // the value matrix indicates the points one will get
  // if the corresponding cell is occupied
  const int value[8][8] = {{3, 2, 2, 2, 2, 2, 2, 3}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {3, 2, 2, 2, 2, 2, 2, 3}};
  int points = 0;
  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 8; ++j) {
      if (board.Get(i, j) == player)  // if I occupy this cell
        points += value[i][j];
      else if (board.Get(i, j) ==
               GetOpposite(player))  // if the opponent occupies this cell
        points -= value[i][j];
    }
  }
  return points;
}

Position Play(Board board) {
  auto player = board.CurPlayer();       // player is the current player
  auto pos = board.GetAvailableMoves();  // pos contains all valid moves
  Position best_move;  // best_move is the move which leads to maximum profit
  int best_eval = -999999;  // the maximum points so far1
  for (Position p : pos) {  // iterate over the valid moves
    Board board2 = board;   // board2 is a copy of board
    board2.Place(p);        // place at p
    if (Evaluate(board2, player) >
        best_eval) {  // if the move leads to hight points
      best_eval = Evaluate(board2, player);  // update the maximum
      best_move = p;                         // update the optimal move
    }
  }
  return best_move;  // return the optimal move
}
