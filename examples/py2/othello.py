BLACK = 0
WHITE = 1
EMPTY = 2

def get_opposite(c):
  return WHITE if c == BLACK else BLACK

class Board:
  def __init__(self):
    self.cur_player_ = BLACK
    self.board_ = [[EMPTY for i in range(8)] for j in range(8)]
    self.board_[3][3] = self.board_[4][4] = WHITE
    self.board_[3][4] = self.board_[4][3] = BLACK
    self.skip_ = 0

  def get(self, i, j):
    return self.board_[i][j]

  def valid(self, *args):
    if len(args) == 1:
      r, c = args[0]
    else:
      r, c = args
    return self.in_range(r, c) and len(self.get_direction(r, c)) > 0

  def switch_player(self):
    self.cur_player_ = get_opposite(self.cur_player_)

  def place(self, *args):
    if len(args) == 1:
      r, c = args[0]
    else:
      r, c = args
    self.skip_ = 0
    self.flip(r, c)
    self.board_[r][c] = self.cur_player_
    self.switch_player()

  def get_available_moves(self):
    pos = []
    for i in range(8):
      for j in range(8):
        if self.valid(i, j):
          pos.append((i, j))
    return pos

  def skip(self):
    for r in range(8):
      for c in range(8):
        if self.board_[r][c] == EMPTY and self.valid(r, c):
          return False
    self.skip_ += 1
    self.switch_player()
    return True

  def terminated(self):
    return self.skip_ >= 2

  def winner(self):
    black, white = self.count(BLACK), self.count(WHITE)
    return BLACK if black > white else WHITE

  def count(self, cell):
    return sum([sum([self.board_[i][j] == cell for j in range(8)]) for i in range(8)])

  def cur_player(self):
    return self.cur_player_

  def in_range(self, r, c):
    return r >= 0 and r < 8 and c >= 0 and c < 8

  def get_direction(self, r, c):
    if self.board_[r][c] != EMPTY:
      return []
    dirs = []
    opponent = get_opposite(self.cur_player_)
    for dr in [-1, 0, 1]:
      for dc in [-1, 0, 1]:
        if dr == 0 and dc == 0:
          continue
        cur_r, cur_c = r + dr, c + dc
        step = 0
        while self.in_range(cur_r, cur_c) and self.board_[cur_r][cur_c] == opponent:
          cur_r += dr
          cur_c += dc
          step += 1
        if self.in_range(cur_r, cur_c) and self.board_[cur_r][cur_c] == self.cur_player_:
          if step > 0:
            dirs.append((dr, dc, step))
    return dirs

  def flip(self, r, c):
    dirs = self.get_direction(r, c)
    for d in dirs:
      dr, dc, step = d
      cur_r, cur_c = r + dr, c + dc
      for _ in range(step):
        self.board_[cur_r][cur_c] = self.cur_player_
        cur_r += dr
        cur_c += dc

  def __repr__(self):
    BLACK_PIECE = "○"
    WHITE_PIECE = "●"
    BOADER = "  a b c d e f g h "
    res = BOADER + '\n'
    for i in range(8):
      res += str(i + 1) + ' '
      for j in range(8):
        cell = self.get(i, j)
        res += (BLACK_PIECE if cell == BLACK else WHITE_PIECE if cell == WHITE else " ") + ' '
      res += str(i + 1) + '\n'
    res += BOADER + '\n'
    return res

