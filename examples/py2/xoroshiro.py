class Xoroshiro128:
  def __init__(self, a=1, b=2, c=3, d=4):
    self.s = [a, b, c, d]

  def rotl(self, x, k):
    return ((x << k) | (x >> (32 - k))) & 4294967295

  def __call__(self):
    result = (self.rotl((self.s[0] + self.s[3]) & 4294967295, 7) + self.s[0]) & 4294967295
    t = (self.s[1] << 9) & 4294967295
    self.s[2] ^= self.s[0]
    self.s[3] ^= self.s[1]
    self.s[1] ^= self.s[2]
    self.s[0] ^= self.s[3]
    self.s[2] ^= t
    self.s[3] = self.rotl(self.s[3], 11)
    return result
