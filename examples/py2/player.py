import random
import time
import copy

from othello import *

"""
Please write your Othello AI here.
Please do not touch any other functions.
"""

# This is a greedy AI which greedily choose the move 
# which leads to the maximum value defined by the evaluate() function.

def evaluate(board, player):
  # the value matrix indicates the points one will get 
  # if the corresponding cell is occupied
  value = [
    [3, 2, 2, 2, 2, 2, 2, 3],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [3, 2, 2, 2, 2, 2, 2, 3]
  ]
  points = 0
  for i in range(8):
    for j in range(8):
      if board.get(i, j) == player: # if I occupy this cell
        points += value[i][j]
      elif board.get(i, j) == get_opposite(player): # if the opponent occupies this cell
        points -= value[i][j]
  return points

def play(board):
  player = board.cur_player() # player is the current player
  pos = board.get_available_moves() # pos contains all valid moves
  best_move = None # best_move is the move which leads to maximum profit
  best_eval = 0 # the maximum points far
  for p in pos: # iterate over the valid moves
    board2 = copy.deepcopy(board) # board2 is a copy of board
    board2.place(p) # place at p
    if best_move is None or evaluate(board2, player) > best_eval: # if the move leads to higher points
      best_eval = evaluate(board2, player) # update the maximum
      best_move = p # update the optimal move
  return best_move # return the optimal move
