#include <future>
#include <iostream>
#include <random>
#include <string>
#include <utility>
#include <vector>

#include "othello.h"
#include "player.h"
#include "xoroshiro.h"

constexpr int kTimeLimit = 1000;

void PrintMove(int r, int c) {
#ifndef SERVER
  char x = r + '1', y = c + 'a';
  std::cout << x << y << std::endl;
#else
  std::cout << r << ' ' << c << std::endl;
#endif
}

std::pair<int, int> ReadMove() {
#ifndef SERVER
  std::string s;
  std::cin >> s;
  int r = s[0] - '1';
  int c = s[1] - 'a';
  return std::make_pair(r, c);
#else
  int r, c;
  std::cin >> r >> c;
  return std::make_pair(r, c);
#endif
}

void ReportWinner(Board board) {
#ifndef SERVER
  if (board.Winner() == BLACK)
    std::cout << "Black wins!" << std::endl;
  else
    std::cout << "White wins!" << std::endl;
#else
  std::cout << (board.Winner() == BLACK ? 0 : 1) << std::endl;
#endif
}

int main(int argc, const char **argv) {
  if (argc < 2) {
    std::cerr << "[Usage] ./main whoami" << std::endl;
    exit(0);
  }

  Xoroshiro128 rng{};

  Board board{};
  CellType whoami;
  if (atoi(argv[1]) == 0)
    whoami = BLACK;
  else
    whoami = WHITE;

  auto RandMove = [&rng](const Board &board) {
    std::vector<Position> pos;
    for (size_t i = 0; i < 8; ++i) {
      for (size_t j = 0; j < 8; ++j)
        if (board.Valid(i, j)) pos.emplace_back(i, j);
    }
    return pos[rng() % pos.size()];
  };

  auto MainPlay = [&RandMove](Board board) {
    try {
      auto res = Play(board);
      return res;
    } catch (...) {
      return RandMove(board);
    }
  };

  int skip = 0;
#ifndef SERVER
  std::cout << board << std::endl;
#endif
  while (!board.Terminated()) {
    int r = -1, c = -1;
    if (board.Skip()) continue;
    if (board.CurPlayer() == whoami) {
      std::chrono::milliseconds span(kTimeLimit);
      std::future<Position> fut = std::async(MainPlay, board);
      if (fut.wait_for(span) == std::future_status::timeout) {
        std::tie(r, c) = RandMove(board);
      } else {
        std::tie(r, c) = fut.get();
      }
      PrintMove(r, c);
    } else {
      std::tie(r, c) = ReadMove();
    }
    if (!board.Valid(r, c)) {
      std::tie(r, c) = RandMove(board);
#ifndef SERVER
      std::cout << "Invalid action. Randomly place at ";
      PrintMove(r, c);
#endif
    }
    board.Place(r, c);
#ifndef SERVER
    std::cout << board << std::endl;
#endif
  }
  ReportWinner(board);
  return 0;
}
