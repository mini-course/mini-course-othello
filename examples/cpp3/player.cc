#include "player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <utility>
#include <vector>

// This is a greedy AI which greedily choose the move
// which leads to the maximum value defined by the Evaluate() function.

int Evaluate(Board board, CellType player) {
  // the value matrix indicates the points one will get
  // if the corresponding cell is occupied
  const int value[8][8] = {{3, 2, 2, 2, 2, 2, 2, 3}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {2, 1, 1, 1, 1, 1, 1, 2},
                           {2, 1, 1, 1, 1, 1, 1, 2}, {3, 2, 2, 2, 2, 2, 2, 3}};
  int points = 0;
  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 8; ++j) {
      if (board.Get(i, j) == player)  // if I occupy this cell
        points += value[i][j];
      else if (board.Get(i, j) ==
               GetOpposite(player))  // if the opponent occupies this cell
        points -= value[i][j];
    }
  }
  return points;
}

Position Play(Board board) {
  auto player = board.CurPlayer();       // player is the current player
  auto pos = board.GetAvailableMoves();  // pos contains all valid moves
  Position best_move;  // best_move is the move which leads to maximum profit
  int best_eval = -999999;      // the maximum points so far1
  for (Position p : pos) {      // iterate over the valid moves
    Board board2 = board;       // board2 is a copy of board
    board2.Place(p);            // place at p
    if (board2.Terminated()) {  // if the game is terminated
      if (board2.Winner() == player) best_move = p;  // find winning move
      continue;
    }
    auto opponent = board2.CurPlayer();          // opponent player
    auto next_pos = board2.GetAvailableMoves();  // next_pos contains all valid
                                                 // moves for the opponent
    Position best_move_for_opponent;
    int best_eval_for_opponent = -999999;
    for (Position p2 : next_pos) {  // iterate over opponent's valid moves
      Board board3 = board2;        // board3 is a copy of board2
      board3.Place(p2);
      if (Evaluate(board3, opponent) >
          best_eval_for_opponent) {  // simliar to the cpp2 example
        best_eval_for_opponent = Evaluate(board3, opponent);
        best_move_for_opponent = p2;
      }
    }
    if (-best_eval_for_opponent > best_eval) {  // minimax
      best_eval = -best_eval_for_opponent;
      best_move = p;
    }
  }
  return best_move;  // return the optimal move
}
