#ifndef OTHELLO_H_
#define OTHELLO_H_

#include <array>
#include <iostream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

enum CellType : uint8_t { BLACK, WHITE, EMPTY };

inline CellType GetOpposite(CellType c) { return c == BLACK ? WHITE : BLACK; }

struct Position {
  std::array<int, 2> p;

  Position() = default;
  Position(int r, int c) : p{r, c} {}

  int &operator[](int v) { return p[v]; }
  operator std::tuple<int &, int &>() {
    return std::tuple<int &, int &>{p[0], p[1]};
  }
};

class Board {
 public:
  static constexpr size_t kBoardSize = 8;

  Board() : cur_player_(BLACK) {
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) board_[i][j] = EMPTY;
    }
    board_[3][3] = board_[4][4] = WHITE;
    board_[3][4] = board_[4][3] = BLACK;
  }

  CellType Get(int i, int j) const { return board_[i][j]; }

  bool Valid(Position pos) const { return Valid(pos[0], pos[1]); }

  bool Valid(int r, int c) const {
    return InRange(r, c) && !GetDirection(r, c).empty();
  }

  std::vector<Position> GetAvailableMoves() const {
    std::vector<Position> pos;
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) {
        if (Valid(i, j)) pos.emplace_back(i, j);
      }
    }
    return pos;
  }

  void Place(Position pos) { Place(pos[0], pos[1]); }

  void Place(int r, int c) {
    skip_ = 0;
    Flip(r, c);
    board_[r][c] = cur_player_;
    SwitchPlayer();
  }

  bool Skip() {
    for (size_t r = 0; r < kBoardSize; ++r) {
      for (size_t c = 0; c < kBoardSize; ++c) {
        if (board_[r][c] == EMPTY && Valid(r, c)) return false;
      }
    }
    skip_ += 1;
    SwitchPlayer();
    return true;
  }

  CellType Winner() const {
    int black = Count(BLACK);
    int white = Count(WHITE);
    return black > white ? BLACK : WHITE;
  }

  CellType CurPlayer() const { return cur_player_; }

  bool Terminated() const { return skip_ >= 2; }

  int Count(CellType cell) const {
    int res = 0;
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) res += board_[i][j] == cell;
    }
    return res;
  }

  friend std::ostream &operator<<(std::ostream &os, const Board &board) {
    static const std::string kWhitePiece = "●";
    static const std::string kBlackPiece = "○";
    static const std::string kBoader = "  a b c d e f g h ";
    os << kBoader << '\n';
    for (size_t i = 0; i < Board::kBoardSize; ++i) {
      os << i + 1 << ' ';
      for (size_t j = 0; j < Board::kBoardSize; ++j) {
        CellType cell = board.Get(i, j);
        os << (cell == BLACK ? kBlackPiece : cell == WHITE ? kWhitePiece : " ")
           << ' ';
      }
      os << i + 1 << '\n';
    }
    os << kBoader << '\n';
    return os;
  }

 private:
  std::array<std::array<CellType, kBoardSize>, kBoardSize> board_;
  CellType cur_player_;
  uint8_t skip_;

  bool InRange(int r, int c) const {
    return r >= 0 && r < kBoardSize && c >= 0 && c < kBoardSize;
  }

  std::vector<std::tuple<int, int, uint8_t>> GetDirection(int r, int c) const {
    if (board_[r][c] != EMPTY) return {};
    std::vector<std::tuple<int, int, uint8_t>> dir;
    CellType opponent = GetOpposite(cur_player_);
    for (int dr : {-1, 0, 1}) {
      for (int dc : {-1, 0, 1}) {
        if (!dr && !dc) continue;
        int cur_r = r + dr, cur_c = c + dc;
        uint8_t step = 0;
        while (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == opponent) {
          cur_r += dr;
          cur_c += dc;
          step += 1;
        }
        if (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == cur_player_) {
          if (step > 0) dir.emplace_back(dr, dc, step);
        }
      }
    }
    return dir;
  }

  void SwitchPlayer() { cur_player_ = GetOpposite(cur_player_); }

  void Flip(int r, int c) {
    auto dir = GetDirection(r, c);
    for (auto d : dir) {
      int dr, dc;
      uint8_t step;
      std::tie(dr, dc, step) = d;
      int cur_r = r + dr, cur_c = c + dc;
      for (uint8_t i = 0; i < step; ++i) {
        board_[cur_r][cur_c] = cur_player_;
        cur_r += dr;
        cur_c += dc;
      }
    }
  }
};

#endif  // OTHELLO_H_
