#include "player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <utility>
#include <vector>

Position Play(Board board) {
  static std::mt19937 rng(7122);
  // if (rng() & 1) {
  // std::this_thread::sleep_for(std::chrono::seconds(2));
  // }
  std::vector<Position> choice;
  for (size_t i = 0; i < 8; ++i) {
    for (size_t j = 0; j < 8; ++j) {
      if (board.Valid(i, j)) choice.emplace_back(i, j);
    }
  }
  std::shuffle(choice.begin(), choice.end(), rng);
  return choice[0];
}
