#include <future>
#include <iostream>
#include <random>
#include <string>
#include <utility>
#include <vector>

#include "othello.h"
#include "player.h"
#include "xoroshiro.h"

constexpr int kTimeLimit = 1000;

std::pair<int, int> ReadMove() {
#ifdef DEBUG
  std::string s;
  std::cin >> s;
  int r = s[0] - '1';
  int c = s[1] - 'a';
  return std::make_pair(r, c);
#else
  int r, c;
  std::cin >> r >> c;
  return std::make_pair(r, c);
#endif
}

int main(int argc, const char **argv) {
  if (argc < 2) {
    std::cerr << "[Usage] ./main whoami" << std::endl;
    exit(0);
  }

  Xoroshiro128 rng{};

  Board board{};
  CellType whoami;
  if (atoi(argv[1]) == 0)
    whoami = BLACK;
  else
    whoami = WHITE;

  auto RandMove = [&rng](const Board &board) {
    std::vector<Position> pos;
    for (size_t i = 0; i < 8; ++i) {
      for (size_t j = 0; j < 8; ++j)
        if (board.Valid(i, j)) pos.emplace_back(i, j);
    }
    return pos[rng() % pos.size()];
  };

  auto MainPlay = [&RandMove](Board board) {
    try {
      auto res = Play(board);
      return res;
    } catch (...) {
      std::cerr << "catch" << std::endl;
      return RandMove(board);
    }
  };

  int skip = 0;
  while (!board.Terminated()) {
    size_t r, c;
    if (board.ForceSkip()) {
      board.Skip();
      continue;
    }
    skip = 0;
    if (board.CurPlayer() == whoami) {
      std::chrono::milliseconds span(kTimeLimit);
      std::future<Position> fut = std::async(MainPlay, board);
      if (fut.wait_for(span) == std::future_status::timeout) {
        std::cerr << "Timeout" << std::endl;
        std::tie(r, c) = RandMove(board);
      } else {
        std::tie(r, c) = fut.get();
      }
      std::cout << r << ' ' << c << std::endl;
      std::cerr << r << ' ' << c << std::endl;
    } else {
      std::tie(r, c) = ReadMove();
    }
    if (!board.Valid(r, c)) std::tie(r, c) = RandMove(board);
    board.Place(r, c);
#ifdef DEBUG
    std::cout << board << std::endl;
#endif
  }
  std::cerr << board.Winner() << std::endl;
  return 0;
}
