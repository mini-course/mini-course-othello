/*****************************************
 * Please do not touch the following codes
 * ***************************************/
#include <array>
#include <future>
#include <iostream>
#include <random>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace othello {

class Xoroshiro128 {
 public:
  using result_type = uint32_t;
  static constexpr result_type(min)() { return 0; }
  static constexpr result_type(max)() { return UINT32_MAX; }
  static inline result_type rotl(const result_type x, int k) {
    return (x << k) | (x >> (32 - k));
  }
  Xoroshiro128() : Xoroshiro128(1, 2, 3, 4) {}
  Xoroshiro128(result_type a, result_type b, result_type c, result_type d)
      : s{a, b, c, d} {}
  result_type operator()() {
    const result_type result = rotl(s[0] + s[3], 7) + s[0];
    const result_type t = s[1] << 9;
    s[2] ^= s[0];
    s[3] ^= s[1];
    s[1] ^= s[2];
    s[0] ^= s[3];
    s[2] ^= t;
    s[3] = rotl(s[3], 11);
    return result;
  }

 private:
  std::array<result_type, 4> s;
};

enum CellType : uint8_t { BLACK, WHITE, EMPTY };

CellType GetOpposite(CellType c) { return c == BLACK ? WHITE : BLACK; }

using Position = std::pair<size_t, size_t>;

class Board {
 public:
  static constexpr size_t kBoardSize = 8;

  Board() : cur_player_(BLACK) {
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) board_[i][j] = EMPTY;
    }
    board_[3][3] = board_[4][4] = WHITE;
    board_[3][4] = board_[4][3] = BLACK;
  }

  CellType Get(size_t i, size_t j) const { return board_[i][j]; }

  bool Valid(size_t r, size_t c) const {
    return InRange(r, c) && !GetDirection(r, c).empty();
  }

  std::vector<Position> GetAvailableMoves() const {
    std::vector<Position> pos;
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) {
        if (Valid(i, j)) pos.emplace_back(i, j);
      }
    }
    return pos;
  }

  void Place(Position pos) { Place(pos.first, pos.second); }

  void Place(size_t r, size_t c) {
    skip_ = 0;
    Flip(r, c);
    board_[r][c] = cur_player_;
    SwitchPlayer();
  }

  bool ForceSkip() const {
    for (size_t r = 0; r < kBoardSize; ++r) {
      for (size_t c = 0; c < kBoardSize; ++c) {
        if (board_[r][c] == EMPTY && Valid(r, c)) return false;
      }
    }
    return true;
  }

  void Skip() {
    skip_ += 1;
    SwitchPlayer();
  }

  CellType Winner() const {
    uint8_t cnt[2] = {0, 0};
    for (size_t r = 0; r < kBoardSize; ++r) {
      for (size_t c = 0; c < kBoardSize; ++c) {
        if (board_[r][c] != EMPTY) cnt[board_[r][c]] += 1;
      }
    }
    if (cnt[0] > cnt[1])
      return BLACK;
    else
      return WHITE;
  }

  CellType CurPlayer() const { return cur_player_; }

  bool Terminated() const { return skip_ == 2; }

  friend std::ostream &operator<<(std::ostream &os, const Board &board) {
    static const std::string kWhitePiece = "●";
    static const std::string kBlackPiece = "○";
    os << "  a b c d e f g h \n";
    for (size_t i = 0; i < Board::kBoardSize; ++i) {
      os << i + 1 << ' ';
      for (size_t j = 0; j < Board::kBoardSize; ++j) {
        CellType cell = board.Get(i, j);
        os << (cell == BLACK ? kBlackPiece : cell == WHITE ? kWhitePiece : " ")
           << ' ';
      }
      os << i + 1 << '\n';
    }
    os << "  a b c d e f g h \n";
    return os;
  }

 private:
  std::array<std::array<CellType, kBoardSize>, kBoardSize> board_;
  CellType cur_player_;
  uint8_t skip_;

  bool InRange(int r, int c) const {
    return r >= 0 && r < kBoardSize && c >= 0 && c < kBoardSize;
  }

  std::vector<std::tuple<int, int, uint8_t>> GetDirection(size_t r,
                                                          size_t c) const {
    if (board_[r][c] != EMPTY) return {};
    std::vector<std::tuple<int, int, uint8_t>> dir;
    CellType opponent = GetOpposite(cur_player_);
    for (int dr : {-1, 0, 1}) {
      for (int dc : {-1, 0, 1}) {
        if (!dr && !dc) continue;
        int cur_r = r + dr, cur_c = c + dc;
        uint8_t step = 0;
        while (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == opponent) {
          cur_r += dr;
          cur_c += dc;
          step += 1;
        }
        if (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == cur_player_) {
          if (step > 0) dir.emplace_back(dr, dc, step);
        }
      }
    }
    return dir;
  }

  void SwitchPlayer() { cur_player_ = GetOpposite(cur_player_); }

  void Flip(size_t r, size_t c) {
    auto dir = GetDirection(r, c);
    for (auto d : dir) {
      int dr, dc;
      uint8_t step;
      std::tie(dr, dc, step) = d;
      int cur_r = r + dr, cur_c = c + dc;
      for (uint8_t i = 0; i < step; ++i) {
        board_[cur_r][cur_c] = cur_player_;
        cur_r += dr;
        cur_c += dc;
      }
    }
  }
};

constexpr int kTimeLimit = 1000;

void PrintMove(int r, int c) {
#ifndef SERVER
  char x = r + '1', y = c + 'a';
  std::cout << x << y << std::endl;
#else
  std::cout << r << ' ' << c << std::endl;
#endif
}

std::pair<int, int> ReadMove() {
#ifndef SERVER
  std::string s;
  std::cin >> s;
  int r = s[0] - '1';
  int c = s[1] - 'a';
  return std::make_pair(r, c);
#else
  int r, c;
  std::cin >> r >> c;
  return std::make_pair(r, c);
#endif
}

void ReportWinner(Board board) {
#ifndef SERVER
  if (board.Winner() == BLACK)
    std::cout << "Black wins!" << std::endl;
  else
    std::cout << "White wins!" << std::endl;
#else
  std::cout << (board.Winner() == BLACK ? 0 : 1) << std::endl;
#endif
}

}  // namespace othello

othello::Position Play(othello::Board board);

int main(int argc, const char **argv) {
  if (argc < 2) {
    std::cerr << "[Usage] ./main whoami" << std::endl;
    exit(0);
  }

  using namespace othello;

  Xoroshiro128 rng{};

  Board board{};
  CellType whoami;
  if (atoi(argv[1]) == 0)
    whoami = BLACK;
  else
    whoami = WHITE;

  auto RandMove = [&rng](const Board &board) {
    std::vector<Position> pos;
    for (size_t i = 0; i < 8; ++i) {
      for (size_t j = 0; j < 8; ++j)
        if (board.Valid(i, j)) pos.emplace_back(i, j);
    }
    return pos[rng() % pos.size()];
  };

  auto MainPlay = [&RandMove](Board board) {
    try {
      auto res = Play(board);
      return res;
    } catch (...) {
      std::cerr << "catch" << std::endl;
      return RandMove(board);
    }
  };

  int skip = 0;
  while (!board.Terminated()) {
    size_t r, c;
    if (board.ForceSkip()) {
      board.Skip();
      continue;
    }
    skip = 0;
    if (board.CurPlayer() == whoami) {
      std::chrono::milliseconds span(kTimeLimit);
      std::future<Position> fut = std::async(MainPlay, board);
      if (fut.wait_for(span) == std::future_status::timeout) {
        std::tie(r, c) = RandMove(board);
      } else {
        std::tie(r, c) = fut.get();
      }
      PrintMove(r, c);
    } else {
      std::tie(r, c) = ReadMove();
    }
    if (!board.Valid(r, c)) {
      std::tie(r, c) = RandMove(board);
#ifndef SERVER
      std::cout << "Invalid action. Randomly place at ";
      PrintMove(r, c);
#endif
    }
    board.Place(r, c);
#ifndef SERVER
    std::cout << board << std::endl;
#endif
  }
  ReportWinner(board);
  return 0;
}

/*****************************************
 * Please write your othello AI here.
 * ***************************************/

using othello::Board;
using othello::Position;
using othello::CellType;

constexpr int kDepth = 3;
constexpr int kInf = 1'000'000'000;
constexpr int kIteration = 20;

namespace {

int Simulate(Board board) {
  static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  int sum = 0;
  CellType player = board.CurPlayer();
  for (int it = 0; it < kIteration; ++it) {
    Board cur = board;
    while (!board.Terminated()) {
      if (board.ForceSkip()) {
        board.Skip();
      } else {
        auto pos = board.GetAvailableMoves();
        board.Place(pos[rng() % pos.size()]);
      }
    }
    if (board.Winner() == player)
      sum += 1;
    else
      sum -= 1;
  }
  return sum;
}

std::pair<Position, int> Dfs(Board board, int depth) {
  if (depth == kDepth || board.Terminated())
    return std::make_pair(Position(0, 0), Simulate(board));
  int best = -kInf;
  Position opt;
  auto v = board.GetAvailableMoves();
  if (v.empty()) {
    board.ForceSkip();
    auto res = Dfs(board, depth + 1);
    return make_pair(Position(0, 0), -res.second);
  }
  for (auto pos : v) {
    Board nxt = board;
    nxt.Place(pos);
    auto res = Dfs(nxt, depth + 1);
    if (-res.second > best) {
      best = -res.second;
      opt = pos;
    }
  }
  return std::make_pair(opt, best);
}

}  // namespace

Position Play(Board board) {
  auto res = Dfs(board, 0);
  return res.first;
}
