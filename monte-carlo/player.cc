#include "player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <utility>
#include <vector>

constexpr int kDepth = 3;
constexpr int kInf = 1'000'000'000;
constexpr int kIteration = 20;

namespace {

int Simulate(Board board) {
  static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  int sum = 0;
  CellType player = board.CurPlayer();
  for (int it = 0; it < kIteration; ++it) {
    Board cur = board;
    while (!board.Terminated()) {
      if (board.ForceSkip()) {
        board.Skip();
      } else {
        auto pos = board.GetAvailableMoves();
        board.Place(pos[rng() % pos.size()]);
      }
    }
    if (board.Winner() == player)
      sum += 1;
    else
      sum -= 1;
  }
  return sum;
}

std::pair<Position, int> Dfs(Board board, int depth) {
  if (depth == kDepth || board.Terminated())
    return std::make_pair(Position(0, 0), Simulate(board));
  int best = -kInf;
  Position opt;
  auto v = board.GetAvailableMoves();
  if (v.empty()) {
    board.ForceSkip();
    auto res = Dfs(board, depth + 1);
    return make_pair(Position(0, 0), -res.second);
  }
  for (auto pos : v) {
    Board nxt = board;
    nxt.Place(pos);
    auto res = Dfs(nxt, depth + 1);
    if (-res.second > best) {
      best = -res.second;
      opt = pos;
    }
  }
  return std::make_pair(opt, best);
}

}  // namespace

Position Play(Board board) {
  auto res = Dfs(board, 0);
  return res.first;
}
