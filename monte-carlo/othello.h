#ifndef OTHELLO_H_
#define OTHELLO_H_

#include <array>
#include <iostream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

enum CellType : uint8_t { BLACK, WHITE, EMPTY };

inline CellType GetOpposite(CellType c) { return c == BLACK ? WHITE : BLACK; }

using Position = std::pair<size_t, size_t>;

class Board {
 public:
  static constexpr size_t kBoardSize = 8;

  Board() : cur_player_(BLACK) {
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) board_[i][j] = EMPTY;
    }
    board_[3][3] = board_[4][4] = WHITE;
    board_[3][4] = board_[4][3] = BLACK;
  }

  CellType Get(size_t i, size_t j) const { return board_[i][j]; }

  bool Valid(size_t r, size_t c) const { return !GetDirection(r, c).empty(); }

  std::vector<Position> GetAvailableMoves() const {
    std::vector<Position> pos;
    for (size_t i = 0; i < kBoardSize; ++i) {
      for (size_t j = 0; j < kBoardSize; ++j) {
        if (Valid(i, j)) pos.emplace_back(i, j);
      }
    }
    return pos;
  }

  void SwitchPlayer() { cur_player_ = GetOpposite(cur_player_); }

  void Place(Position pos) { Place(pos.first, pos.second); }

  void Place(size_t r, size_t c) {
    skip_ = 0;
    Flip(r, c);
    board_[r][c] = cur_player_;
    SwitchPlayer();
  }

  bool ForceSkip() const {
    for (size_t r = 0; r < kBoardSize; ++r) {
      for (size_t c = 0; c < kBoardSize; ++c) {
        if (board_[r][c] == EMPTY && Valid(r, c)) return false;
      }
    }
    return true;
  }

  void Skip() {
    skip_ += 1;
    SwitchPlayer();
  }

  CellType Winner() const {
    uint8_t cnt[2] = {0, 0};
    for (size_t r = 0; r < kBoardSize; ++r) {
      for (size_t c = 0; c < kBoardSize; ++c) {
        if (board_[r][c] != EMPTY) cnt[board_[r][c]] += 1;
      }
    }
    if (cnt[0] > cnt[1])
      return BLACK;
    else
      return WHITE;
  }

  CellType CurPlayer() const { return cur_player_; }

  bool Terminated() const { return skip_ == 2; }

  friend std::ostream &operator<<(std::ostream &os, const Board &board) {
    os << "  a b c d e f g h \n";
    for (size_t i = 0; i < Board::kBoardSize; ++i) {
      os << i + 1 << ' ';
      for (size_t j = 0; j < Board::kBoardSize; ++j) {
        std::string x = board.Get(i, j) == BLACK
                            ? "○"
                            : board.Get(i, j) == WHITE ? "●" : " ";
        os << x << ' ';
      }
      os << i + 1 << '\n';
    }
    os << "  a b c d e f g h \n";
    return os;
  }

 private:
  std::array<std::array<CellType, kBoardSize>, kBoardSize> board_;
  CellType cur_player_;
  uint8_t skip_;

  bool InRange(int r, int c) const {
    return r >= 0 && r < kBoardSize && c >= 0 && c < kBoardSize;
  }

  std::vector<std::tuple<int, int, uint8_t>> GetDirection(size_t r,
                                                          size_t c) const {
    if (board_[r][c] != EMPTY) return {};
    std::vector<std::tuple<int, int, uint8_t>> dir;
    CellType opponent = GetOpposite(cur_player_);
    for (int dr : {-1, 0, 1}) {
      for (int dc : {-1, 0, 1}) {
        if (!dr && !dc) continue;
        int cur_r = r + dr, cur_c = c + dc;
        uint8_t step = 0;
        while (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == opponent) {
          cur_r += dr;
          cur_c += dc;
          step += 1;
        }
        if (InRange(cur_r, cur_c) && board_[cur_r][cur_c] == cur_player_) {
          if (step > 0) dir.emplace_back(dr, dc, step);
        }
      }
    }
    return dir;
  }

  void Flip(size_t r, size_t c) {
    auto dir = GetDirection(r, c);
    for (auto d : dir) {
      auto [dr, dc, step] = d;
      int cur_r = r + dr, cur_c = c + dc;
      for (uint8_t i = 0; i < step; ++i) {
        board_[cur_r][cur_c] = cur_player_;
        cur_r += dr;
        cur_c += dc;
      }
    }
  }
};

#endif  // OTHELLO_H_
