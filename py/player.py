import random
import time

"""
Please write your Othello AI here.
Please do not touch any other functions.
"""

def play(board):
  pos = board.get_available_moves()
  return pos[0]
